#! /usr/bin/env python3

# load required modules
import argparse
import re
import csv


def parse_args():
	# create an ArgumentParser object
	parser = argparse.ArgumentParser(description = 'this script parses an ugly data file')

	# adding arguments
	parser.add_argument("csv", help="untidy, ugly csv output by the software")

	return parser.parse_args()


def parse_data():
	file = open(arguments.csv, 'r')

	# slurp the entire file into a string
	file_string = file.read()

	# close the data file
	file.close()

	# split into a list where each item is a different date with donations
	dates = file_string.split('Date Occurred: ')

	# print header line
	print(','.join(['date', 'budget_code', 'description', 'amount']))

	# loop over all of the dates
	for date in dates[1:]:

		# split this big donation block into a set of lines
		lines = date.splitlines()

		# parse each csv line
		reader = csv.reader(lines)

		# loop over each field in the csv line we just split
		for field in reader:
			# field[0] only defined if it's the date line
			if field[0]:
				date_string = field[0]

			# field[2] only defined if it's a donation line
			elif field[2]:
				# split the donation field to get the 3-digit code and the description
				a = field[2].split(' - ')
				code = a[0]
				description = '-'.join(a[1:])
				description = description.replace(' ', '-')
				amount = field[7].replace(',','')

				# print the output
				print(','.join([date_string, code, description, amount]))


def main():
	parse_data()


arguments = parse_args()

# set the environment for this script
# is it main, or is this a module being called by another script
if __name__ == '__main__':
	main()
