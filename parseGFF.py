#! /usr/bin/env python3
import csv
import argparse

def get_args():
    parser = argparse.ArgumentParser(description = 'This script parses covid.fasta')
    parser.add_argument("fasta", help="The FASTA file with the vovid genome data")
    parser.add_argument("gff3", help="The gff3 file with each covid gene data")
    args = parser.parse_args()
    return args
    
# setting the file
#fasta_file = 'covid.fasta'
# read in the genome file and store the sequence
def fasta_read(fasta):
    genome_sequence = ''
    with open(fasta, 'r') as FASTA:
        # skip the header line
        next(FASTA)
        # loop over all lines in the file	
        for line in FASTA:
            # remove the line breaks
            line = line.rstrip()
            genome_sequence += line
    # Part 2 and 3
    STDOUT = len(genome_sequence)
    print('Genome size:', STDOUT, 'nucleotides')
    print("Writting FASTA file to 'covid_genes.fasta'")
    return genome_sequence

# Setting the file
#covid19_genome = 'covid_genes.gff3'
def gene_write(gff3, genefile):
    genename = []
    genesequence = []

    with open(gff3, 'r') as i:
        # Using the CSV import to parse the file at tab
        reader = csv.reader(i, delimiter='\t')
        for line in reader:
            # If and only if the 3rd column is = to gene 
            if (line[2] == 'gene'):

                # creating integers from the 4th and 5th columns
                start = int(line[3])
                end = int(line[4])
                # assigning a variable for the names of said genes
                gene_name = line[8]
                # length of each gene
                length = ((end - start)+1)
                print(length)
                # sequence of each gene 
                seq = genefile[start-1:end]
                print(seq)
                # replacing the ID= output with >
                replacein = gene_name.replace('ID=', '>')
                print(replacein)
                # starts new line
                spaces = '\n'
                # we are writing to the covid_genes.fasta with the variable from above
                with open("covid_genes.fasta", 'w') as fastafile:
                    fastafile.write(str(replacein))
                    fastafile.write(str(spaces))
                    fastafile.write(str(seq))
                    fastafile.write(str(spaces))


def main():
    genefile = fasta_read(arguments.fasta)
    gene_write (arguments.gff3, genefile)

arguments = get_args()
if __name__ == '__main__':
    main()